# Lirc driver for xbox

*Why ?*

The Xbox DVD IR receiver can be connected to a computer while involving a little soldering. It is described [here](https://www.mythtv.org/wiki/XBOX_DVD_IR_Receiver) and [here](https://kodi.wiki/view/HOW-TO:Wire_your_XBOX_DVD-Remote_for_USB).
A long time ago, [lirc](http://www.lirc.org/) was distributing kernel drivers of many receivers. The driver of this receiver is called `lirc_xbox` and was part of `lirc`. Efforts has been put to bring to the kernel team thoses drivers but the xbox one didn't find its way. Unfortunately, the `lirc` team also removed it from their sources and thus this dongle couldn't be setup easily with a recent version (0.9+).

*Solution*

I gathered the [source driver](https://github.com/Albinoman887/lirc/blob/master/drivers/lirc_xbox/lirc_xbox.c), add the [configuration for DKMS](http://xmodulo.com/build-kernel-module-dkms-linux.html) and add this manual to bring this dongle back alive.

## Prerequisite

`dkms` is needed to automatically build and install this module after each kernel update
```
sudo apt install dkms
```

## Install

Get the sources (the name of the folder is important for next part :
```
mkdir /usr/src/lirc_xbox-1.88
cd /usr/src/lirc_xbox-1.88
git clone https://gitlab.com/coliss86/lirc_xbox.git .

```

Make sure the driver compiles : 

```
make
```

You should have the following output

```
make -C /lib/modules/4.4.0-131-generic/build M=/usr/src/lirc_xbox-1.88 modules
make[1]: Entering directory '/usr/src/linux-headers-4.4.0-131-generic'
  CC [M]  /usr/src/lirc_xbox-1.88/lirc_xbox.o
  Building modules, stage 2.
  MODPOST 1 modules
  CC      /usr/src/lirc_xbox-1.88/lirc_xbox.mod.o
  LD [M]  /usr/src/lirc_xbox-1.88/lirc_xbox.ko
make[1]: Leaving directory '/usr/src/linux-headers-4.4.0-131-generic'
```

Clean the directory : 
```
make clean
```

Now add it to the `dkms` registry :
```
sudo dkms add lirc_xbox/1.88
```

You should have the following output :
```
Creating symlink /var/lib/dkms/lirc_xbox/1.88/source ->
                 /usr/src/lirc_xbox-1.88

DKMS: add completed.
```

Next build it : 
```
sudo dkms build lirc_xbox/1.88
```

You should have the following output :
```
Kernel preparation unnecessary for this kernel.  Skipping...

Building module:
cleaning build area....
make KERNELRELEASE=4.4.0-131-generic -C /lib/modules/4.4.0-131-generic/build M=/var/lib/dkms/lirc_xbox/1.88/build....
cleaning build area....

DKMS: build completed.
```

Install the driver in the kernel tree : 
```
sudo dkms install lirc_xbox/1.88
```

you should have the following output :
```
lirc_xbox:
Running module version sanity check.
 - Original module
   - No original module exists within this kernel
 - Installation
   - Installing to /lib/modules/4.4.0-131-generic/updates/dkms/

depmod....

DKMS: install completed.
```

Add this line to the bottom of `/etc/modprobe.d/blacklist.conf` :
```
blacklist xpad
```

Remove if necessary the `xpad` module
```
sudo rmmod xpad
```

Load the module : 
```
sudo modprobe lirc_xbox
```

Connect the receiver and check kernel messages : 
```
sudo journalctl -k | tail
```

```
juil. 29 23:10:07 olive kernel: lirc_dev: IR Remote Control driver registered, major 243 
juil. 29 23:46:46 olive kernel: 
                                lirc_xbox: XBOX DVD Dongle USB remote driver for LIRC $Revision: 0.01 $
juil. 29 23:46:46 olive kernel: lirc_xbox: Jason Martin <austinspartan@users.sourceforge.net>
juil. 29 23:46:46 olive kernel: lirc_xbox 3-1.3.3:1.0: lirc_dev: driver lirc_xbox  registered at minor = 0
juil. 29 23:46:46 olive kernel: lirc_xbox[15]:  on usb3:15
juil. 29 23:46:46 olive kernel: usbcore: registered new interface driver lirc_xbox
```

Bingo !!!
Now you can configure `lirc` as usual

